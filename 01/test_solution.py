import pytest

from solution import get_fuel_needed, get_fuel_fuel


@pytest.mark.parametrize(
    "mass, fuel_required", [(12, 2), (14, 2), (1969, 654), (100756, 33583)]
)
def test_get_fuel_needed(mass, fuel_required):
    calc_fuel_required = get_fuel_needed(mass)
    assert fuel_required == calc_fuel_required


@pytest.mark.parametrize(
    "mass, fuel_fuel", [(12, 2), (14, 2), (1969, 966), (100756, 50346)]
)
def test_get_fuel_fuel(mass, fuel_fuel):
    calc_fuel_fuel = get_fuel_fuel(mass)
    assert fuel_fuel == calc_fuel_fuel
