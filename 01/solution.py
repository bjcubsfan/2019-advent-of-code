#!/usr/bin/env python


def get_fuel_needed(mass):
    fuel_needed = (mass // 3) - 2
    return fuel_needed


def get_fuel_fuel(mass):
    fuel_fuel = 0
    while mass >= 0:
        fuel_for_this_mass = get_fuel_needed(mass)
        # print(f"Fuel for this mass is {fuel_for_this_mass}")
        if fuel_for_this_mass <= 0:
            return fuel_fuel
        else:
            fuel_fuel += fuel_for_this_mass
            mass = fuel_for_this_mass


def main():
    with open("input") as input_data:
        total_fuel = 0
        fuel_fuel = 0
        for line in input_data:
            total_fuel += get_fuel_needed(int(line))
            fuel_fuel += get_fuel_fuel(int(line))
    print(f"Fuel needed is {total_fuel}")
    print(f"Fuel fuel needed is {fuel_fuel}")


if __name__ == "__main__":
    main()
