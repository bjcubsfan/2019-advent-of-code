#!/usr/bin/env python


def get_points(wire):
    points = set()
    loc = (0, 0)
    for coordinate in wire.split(","):
        print(coordinate)
        direction, magnitude = coordinate[0], int(coordinate[1:])
        if direction == "R":
            for _ in range(magnitude):
                loc = (loc[0] + 1, loc[1])
                points.add(loc)
        elif direction == "L":
            for _ in range(magnitude):
                loc = (loc[0] - 1, loc[1])
                points.add(loc)
        elif direction == "U":
            for _ in range(magnitude):
                loc = (loc[0], loc[1] + 1)
                points.add(loc)
        elif direction == "D":
            for _ in range(magnitude):
                loc = (loc[0], loc[1] - 1)
                points.add(loc)
    return points


def get_min_crossing_distance(wire_1_points, wire_2_points):
    in_both = wire_1_points & wire_2_points
    print(in_both)
    distances = [abs(point[0]) + abs(point[1]) for point in in_both]
    return min(distances)


def crossing_distance(paths):
    print("*****")
    wire_1, wire_2 = paths.split("\n")
    wire_1_points = get_points(wire_1.strip())
    wire_2_points = get_points(wire_2.strip())
    crossing_distance = get_min_crossing_distance(wire_1_points, wire_2_points)
    return crossing_distance


def main():
    with open("input") as input_data:
        print(crossing_distance(input_data.read().strip()))


if __name__ == "__main__":
    main()
