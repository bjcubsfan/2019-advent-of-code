#!/usr/bin/env python


def run_intcode(input_intcode):
    code = input_intcode
    pos = 0
    while True:
        if code[pos] == 1:
            idx_add_1 = code[pos + 1]
            idx_add_2 = code[pos + 2]
            dest = code[pos + 3]
            # print(f"Adding, {code[idx_add_1]} + {code[idx_add_2]}, dest is {dest}")
            code[dest] = code[idx_add_1] + code[idx_add_2]
            pos = pos + 4
        elif input_intcode[pos] == 2:
            idx_mul_1 = code[pos + 1]
            idx_mul_2 = code[pos + 2]
            dest = code[pos + 3]
            # print(f"Multiplying, {code[idx_mul_1]} * {code[idx_mul_2]}, dest is {dest}")
            code[dest] = code[idx_mul_1] * code[idx_mul_2]
            pos = pos + 4
        elif input_intcode[pos] == 99:
            return code
        else:
            raise ValueError(f"Invalid intcode {code[pos]}")


def main():
    with open("input") as input_data:
        for line in input_data:
            string_intcodes = line.split(",")
    intcodes = [int(code) for code in string_intcodes]
    intcodes[1] = 12
    intcodes[2] = 2
    code = run_intcode(intcodes)
    print(f"The value at 0 is {code[0]}")
    for noun in range(100):
        if noun % 10 == 0:
            print(f"Trying noun {noun}")
        for verb in range(100):
            intcodes = [int(code) for code in string_intcodes]
            intcodes[1] = noun
            intcodes[2] = verb
            code = run_intcode(intcodes)
            if code[0] == 19690720:
                print(f"noun is {noun}, verb is {verb}, answer is {100 * noun + verb}")
                return
    print(f"Last noun {noun}, last verb {verb}")


if __name__ == "__main__":
    main()
